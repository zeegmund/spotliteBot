// Copyright (C) 2019 ZEEGA
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


package main

import (
	"flag"
	"log"
	"os"
	
	"github.com/Syfaro/telegram-bot-api"
	"github.com/go-redis/redis"
)

// variable for token
var token, reply string


func init() {
	// accepting flag -token
	flag.StringVar(&token, "token", <token>, "Telegram Bot Token")
	flag.Parse()

	// no start without flag
	if token == "" {
		log.Print("-token is required")
		os.Exit(1)
	}
}

func main() {
	// creating new instance using token
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	// u - structure with configuration for updates
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	// creating channel for messaged using configutation 'u'
	updates, err := bot.GetUpdatesChan(u)

	// channel 'update' gets 'Update' type structures
	// structures reading and proccesing
	for update := range updates {
		// universal answer
		reply = "You can use me only at the @intelligentu.\n\n[Help](tg://user?id=343831535)/[Code](gitlab.com/zeegmund/spotliteBot)?"
		if update.Message == nil {
			continue
		}

		// logging sender and message
		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		// switch for command proccesing
		// command - message starting with "/"
		switch update.Message.Command() {
		case "all":
			if (update.Message.Chat.Type == "group" || update.Message.Chat.Type == "supergroup") {
				// mentioning the all users of group
				var (
					text				string
					chatMember			tgbotapi.ChatMember
					chatConfigWithUser	tgbotapi.ChatConfigWithUser
				)
				chatConfig := tgbotapi.ChatConfig{update.Message.Chat.ID, "@"+update.Message.Chat.UserName}
				chatMembersCount, err := bot.GetChatMembersCount(chatConfig)
				if err != nil {
					log.Panic(err)
				}
				for i:=0; i<chatMembersCount; i++ {
					chatConfigWithUser = tgbotapi.ChatConfigWithUser{update.Message.Chat.ID, update.Message.Chat.UserName, i}
					chatMember, err = bot.GetChatMember(chatConfigWithUser)
					if err != nil {
						log.Panic(err)
					}
					text = "%s[‎](tg://user?id=%n)"+text+string(chatMember.User.ID)
				}
				reply = text+"Attention, please!"
			} else {
				reply = "You have to use this command at group or supergroup."
			}
		}
		// creating replying message
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, reply)
		msg.ParseMode = "Markdown"
		msg.DisableWebPagePreview = true
		// sending
		bot.Send(msg)
	}
}
